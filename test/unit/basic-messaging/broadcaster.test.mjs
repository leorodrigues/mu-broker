import { expect, use } from 'chai';
import sinon from 'sinon';
import { Broadcaster } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const subscribers = {
    push: sandbox.stub(),
    findAllListeningTo: sandbox.stub()
};

const subscriber = {
    wakeUp: sandbox.stub()
};

describe('Broadcaster', () => {
    describe('#push', () => {
        it('Should collect the given subscriber.', () => {
            const subject = new Broadcaster(subscribers);
            subject.push('s');
            expect(subscribers.push).to.be.calledOnceWithExactly('s');
        });
    });
    describe('#broadcast', () => {
        it('Should wake up all subscribers bound to the given topic.', () => {
            subscribers.findAllListeningTo.returns([ subscriber, subscriber ]);
            const subject = new Broadcaster(subscribers);
            subject.broadcast('s');
            expect(subscribers.findAllListeningTo)
                .to.be.calledOnceWithExactly('s');
            expect(subscriber.wakeUp).to.be.calledTwice
                .and.calledWithExactly()
                .and.calledWithExactly();
        });
    });
});
