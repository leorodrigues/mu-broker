import { expect } from 'chai';
import { SubscriberArray } from '../../../index.mjs';

describe('SubscriberArray', () => {
    describe('#findAllListeningTo', () => {
        it('Should return only the itens that are listening to the topic', () => {
            const subject = new SubscriberArray();
            subject.push(
                { name: 'a', listeningTo: () => false },
                { name: 'b', listeningTo: () => true }
            );

            const result = subject.findAllListeningTo('x').map(e => e.name);
            expect(result).to.be.deep.equal([ 'b' ]);
        });
    });
});
