import { expect, use } from 'chai';
import sinon from 'sinon';
import { MessageQueue } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const topic = { matches: sandbox.stub() };

describe('MessageQueue', () => {
    describe('#acceptsTopic', () => {
        it('Should return the matching of its internal topic instance vs the given one', () => {
            topic.matches.returns('affirmative');
            const result = new MessageQueue(topic).acceptsTopic('the-topic');
            expect(result).to.be.equal('affirmative');
            expect(topic.matches).to.be.calledOnceWithExactly('the-topic');
        });
    });
});
