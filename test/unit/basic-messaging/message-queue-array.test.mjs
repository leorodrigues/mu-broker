import { expect, use } from 'chai';
import sinon from 'sinon';
import { MessageQueueArray } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const messageQueue = {
    acceptsTopic: sandbox.stub(),
    push: sandbox.stub()
};

const message = {
    makeDeepCopy: sandbox.stub()
};

describe('MessageQueueArray', () => {
    describe('#post', () => {
        it('Should return only the itens that are listening to the topic', () => {
            messageQueue.acceptsTopic.onCall(0).returns(false);
            messageQueue.acceptsTopic.onCall(1).returns(true);
            message.makeDeepCopy.returns('fake-copy');

            const subject = new MessageQueueArray();
            subject.push(messageQueue);
            subject.push(messageQueue);
            subject.post('fake-topic', message);

            expect(message.makeDeepCopy)
                .to.be.calledOnceWithExactly();
            expect(messageQueue.push)
                .to.be.calledOnceWithExactly('fake-copy');
            expect(messageQueue.acceptsTopic).to.be.calledTwice
                .and.calledWith('fake-topic')
                .and.calledWith('fake-topic');
        });
    });
});
