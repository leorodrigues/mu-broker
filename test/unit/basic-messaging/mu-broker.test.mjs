import { expect, use } from 'chai';
import sinon from 'sinon';
import { MuBroker } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const broadcaster = {
    push: sandbox.stub(),
    broadcast: sandbox.stub()
};
const messageQueueArray = {
    push: sandbox.stub(),
    post: sandbox.stub()
};
const brokeringKit = {
    newSubscriber: sandbox.stub(),
    newMessageQueue: sandbox.stub()
};

describe('MuBroker', () => {
    afterEach(() => sandbox.reset());

    describe('#subscribe', () => {
        it('Should register a new subscriber', () => {
            brokeringKit.newMessageQueue.returns('fake-message-queue');
            brokeringKit.newSubscriber.returns('fake-subscriber');
            const subject = new MuBroker(broadcaster, messageQueueArray, brokeringKit);
            subject.subscribe('fake-topic', 'fake-receiver');
            expect(brokeringKit.newMessageQueue)
                .to.be.calledOnceWithExactly('fake-topic');
            expect(brokeringKit.newSubscriber)
                .to.be.calledOnceWithExactly('fake-topic', 'fake-receiver', 'fake-message-queue');
            expect(messageQueueArray.push)
                .to.be.calledOnceWithExactly('fake-message-queue');
            expect(broadcaster.push)
                .to.be.calledOnceWithExactly('fake-subscriber');
        });
    });

    describe('#publish', () => {
        it('Should broadcast to interested subscribers', () => {
            const subject = new MuBroker(broadcaster, messageQueueArray, brokeringKit);
            subject.publish('fake-topic', 'fake-message');
            expect(messageQueueArray.post)
                .to.be.calledOnceWithExactly('fake-topic', 'fake-message');
            expect(broadcaster.broadcast)
                .to.be.calledOnceWithExactly('fake-topic');
        });
    });
});
