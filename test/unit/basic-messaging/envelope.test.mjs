import { expect, use } from 'chai';
import sinon from 'sinon';
import { Envelope } from '../../../index.mjs';
import { describe } from 'node:test';

use((await import('sinon-chai')).default);

const random8BytesStringPattern = /^[a-z0-9]{16}$/;

const sandbox = sinon.createSandbox();

const fakeKit = {
    newEnvelope: sandbox.stub()
};

describe('Envelope', function () {

    afterEach(() => sandbox.reset());

    describe('#correlationId', function () {
        describe('Given a custom correlation id', function () {
            it('Should return the content of the "correlationId" header', () => {
                const subject = new Envelope({ headers: { correlationId: 'fake-id' } });
                expect(subject.correlationId).to.be.equal('fake-id');
            });
        });

        describe('Given no custom correlation id', function () {
            it('Should return a random 8 bytes hex sequence as default id', () => {
                const subject = new Envelope({ });
                expect(subject.correlationId)
                    .to.match(random8BytesStringPattern);
            });
        });
    });

    describe('#deriveEnvelope', function () {
        describe('Given a payload', function () {
            describe('And no headers', function () {
                it('Should make an envelope with the payload and a copy of its own headers', () => {
                    fakeKit.newEnvelope.returns('fake-envelope');

                    const subject = new Envelope({
                        payload: 'original-payload',
                        headers: { myHeader: 112358, correlationId: 42 }
                    });

                    const derived = subject.deriveEnvelope(fakeKit, { payload: 'new-payload' });

                    expect(derived)
                        .to.be.equal('fake-envelope');

                    expect(fakeKit.newEnvelope)
                        .to.have.been.calledOnceWithExactly({
                            payload: 'new-payload',
                            headers: { myHeader: 112358, correlationId: 42 }
                        });
                });
            });

            describe('And some headers', function () {
                it('Should produce an envelope with the payload and merged headers', () => {
                    fakeKit.newEnvelope.returns('fake-envelope');

                    const subject = new Envelope({
                        payload: 'original-payload',
                        headers: { myHeader: 112358, correlationId: 42 }
                    });

                    const derived = subject.deriveEnvelope(fakeKit, {
                        payload: 'new-payload',
                        headers: { theirHeader: 132134 }
                    });

                    expect(derived)
                        .to.be.equal('fake-envelope');

                    expect(fakeKit.newEnvelope)
                        .to.have.been.calledOnceWithExactly({
                            payload: 'new-payload',
                            headers: {
                                myHeader: 112358,
                                correlationId: 42,
                                theirHeader: 132134
                            }
                        });
                });
            });
        });

        describe('Given no payload', function () {
            describe('And no headers', function () {
                it('Should produce an envelope with a copy of its own headers', () => {
                    fakeKit.newEnvelope.returns('fake-envelope');

                    const subject = new Envelope({
                        headers: { myHeader: 112358, correlationId: 42 }
                    });

                    const derived = subject.deriveEnvelope(fakeKit);

                    expect(derived)
                        .to.be.equal('fake-envelope');

                    expect(fakeKit.newEnvelope)
                        .to.have.been.calledOnceWithExactly({
                            headers: { myHeader: 112358, correlationId: 42 }
                        });
                });
            });

            describe('And some headers', function () {
                it('Should produce an envelope with merged headers', () => {
                    fakeKit.newEnvelope.returns('fake-envelope');

                    const subject = new Envelope({
                        headers: { myHeader: 112358, correlationId: 42 }
                    });

                    const derived = subject.deriveEnvelope(fakeKit, {
                        headers: { theirHeader: 132134 }
                    });

                    expect(derived)
                        .to.be.equal('fake-envelope');

                    expect(fakeKit.newEnvelope)
                        .to.have.been.calledOnceWithExactly({
                            headers: {
                                myHeader: 112358,
                                correlationId: 42,
                                theirHeader: 132134
                            }
                        });
                });
            });
        });
    });

    describe('#deriveHeaders', function () {
        describe('Given no intersecting attribs between current and new headers', function () {
            describe('And no attributes to be excluded', function () {
                it('Should merge all headers', () => {
                    const subject = new Envelope({
                        headers: { myValue: 112358, correlationId: 42 }
                    });

                    const derived = subject.deriveHeaders({
                        theirValue: 132134
                    });

                    expect(derived).to.be.deep.equal({
                        myValue: 112358,
                        theirValue: 132134,
                        correlationId: 42
                    });
                });
            });

            describe('And some attributes to be excluded', function () {
                it('Should merge headers but skip the given names', () => {
                    const subject = new Envelope({
                        headers: { myValue: 112358, correlationId: 42 }
                    });

                    const derived = subject.deriveHeaders({
                        theirValue: 132134
                    }, [ 'myValue', 'theirValue' ]);

                    expect(derived).to.be.deep.equal({ correlationId: 42 });
                });
            });
        });

        describe('Given some intersecting attribs between current and new headers', function () {
            describe('And no attributes to be excluded', function () {
                it('Should merge all headers overriding old values', () => {
                    const subject = new Envelope({
                        headers: {
                            ourValue: 3.14,
                            myValue: 112358,
                            correlationId: 42
                        }
                    });

                    const derived = subject.deriveHeaders({
                        ourValue: 1.6180,
                        theirValue: 132134
                    });

                    expect(derived).to.be.deep.equal({
                        myValue: 112358,
                        theirValue: 132134,
                        ourValue: 1.6180,
                        correlationId: 42
                    });
                });
            });

            describe('And some attributes to be excluded', function () {
                it('Should merge headers overriding old values but skip the given names', () => {
                    const subject = new Envelope({
                        headers: {
                            ourValue: 3.14,
                            myValue: 112358,
                            correlationId: 42
                        }
                    });

                    const derived = subject.deriveHeaders({
                        ourValue: 1.6180,
                        theirValue: 132134
                    }, [ 'myValue', 'theirValue', 'ourValue' ]);

                    expect(derived).to.be.deep.equal({ correlationId: 42 });
                });
            });

            describe('And the intersection includes the correlation id', function () {
                it('Should keep the original correlation id', () => {
                    const subject = new Envelope({ headers: { correlationId: 42 } });
                    const derived = subject.deriveHeaders({ correlationId: 11 });
                    expect(derived).to.be.deep.equal({ correlationId: 42 });
                });
            });
        });
    });

    describe('#ensureHeader', function () {
        describe('Given the name is found on the input', function () {
            it('Should return the existing value', () => {
                const subject = new Envelope({ headers: { myHeader: 42, correlationId: 112358 } });
                const outcome = subject.ensureHeader('myHeader');
                expect(outcome).to.be.deep.equal({ myHeader: 42, correlationId: 112358 });
            });
        });

        describe('Given the name is missing from the input', function () {
            describe('Given no compute function', function () {
                it('Should produce a random 8 bytes header as hex string', () => {
                    const subject = new Envelope();
                    const outcome = subject.ensureHeader('myHeader');
                    expect(outcome.myHeader).to.match(random8BytesStringPattern);
                });
            });

            describe('Given a compute function', function () {
                it('Should use the output of the compute function', () => {
                    const subject = new Envelope({ headers: { correlationId: 42 } });
                    const outcome = subject.ensureHeader('myHeader', () => 112358);
                    expect(outcome).to.be.deep.equal({ myHeader: 112358, correlationId: 42 });
                });
            });
        });

        describe('Given no compute function', function () {
            it('Should produce a random 8 bytes header as hex string', () => {
                const subject = new Envelope();
                const outcome = subject.ensureHeader('myHeader');
                expect(outcome.myHeader).to.match(random8BytesStringPattern);
            });
        });

        describe('Given a compute function', function () {
            it('Should use the output of the compute function', () => {
                const subject = new Envelope({ headers: { correlationId: 42 } });
                const outcome = subject.ensureHeader('myHeader', () => 112358);
                expect(outcome).to.be.deep.equal({ myHeader: 112358, correlationId: 42 });
            });
        });
    });
});