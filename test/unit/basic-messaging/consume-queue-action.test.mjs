import { expect, use } from 'chai';
import sinon from 'sinon';
import { ConsumeQueueAction } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const receiver = {
    handle: sandbox.stub()
};

const messageQueue = {
    get length() {
        return this._l;
    },
    shift: sandbox.stub(),
    push: sandbox.stub()
};

describe('ConsumeQueueAction', () => {
    afterEach(() => sandbox.reset());

    describe('#fire', () => {
        it('Should just signal the stop of consumption if its queue is empty', async () => {
            messageQueue._l = 0;
            const subject = new ConsumeQueueAction(receiver, messageQueue);
            expect(await subject.fire()).to.be.true;
        });
        it('Should consume one element from the queue and feed it to the receiver', async () => {
            messageQueue._l = 1;
            messageQueue.shift.returns('fake-evt');
            receiver.handle.callsFake((e, a) => a.confirm());
            const subject = new ConsumeQueueAction(receiver, messageQueue);
            expect(await subject.fire()).to.be.false;
            expect(receiver.handle).to.be.calledOnceWith('fake-evt');
            expect(messageQueue.shift).to.be.calledOnceWithExactly();
        });
        it('Should push the message back if acknowledgement is refused', async () => {
            messageQueue._l = 1;
            messageQueue.shift.returns('fake-evt');
            receiver.handle.callsFake((e, a) => a.refuse());
            const subject = new ConsumeQueueAction(receiver, messageQueue);
            expect(await subject.fire()).to.be.false;
            expect(receiver.handle).to.be.calledOnceWith('fake-evt');
            expect(messageQueue.shift).to.be.calledOnceWithExactly();
            expect(messageQueue.push).to.be.calledOnceWithExactly('fake-evt');
        });
    });
});
