import { expect, use } from 'chai';
import sinon from 'sinon';
import { Subscriber } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const topic = { matches: sandbox.stub() };

const trampoline = { spring: sandbox.stub() };

describe('Subscriber', () => {
    describe('#listentingTo', () => {
        it('Should return if it\'s topic matches the given one.', () => {
            topic.matches.returns('yes');
            const subscriber = new Subscriber(topic, trampoline);
            expect(subscriber.listeningTo('another-topic')).to.be.equal('yes');
            expect(topic.matches).to.be.calledOnceWithExactly('another-topic');
        });
    });
    describe('#wakeUp', () => {
        it('Should spring the trampoline.', () => {
            trampoline.spring.returns('some-value');
            const subscriber = new Subscriber(topic, trampoline);
            expect(subscriber.wakeUp()).to.be.equal('some-value');
            expect(trampoline.spring).to.be.calledOnceWithExactly();
        });
    });
});