import { expect } from 'chai';

import { Acknowledgement, AckError } from '../../../index.mjs';

describe('Acknowledgement', () => {
    describe('#confirmed', () => {
        it('Should throw if its internal state is "PENDING"', () => {
            const ack = new Acknowledgement();
            expect(() => ack.confirmed).to.throw(AckError);
        });

        it('Should be false if its internal state is "REFUSED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.REFUSED;
            expect(ack.confirmed).to.be.false;
        });

        it('Should be true if its internal state is "CONFIRMED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.CONFIRMED;
            expect(ack.confirmed).to.be.true;
        });
    });

    describe('#refused', () => {
        it('Should throw if its internal state is "PENDING"', () => {
            const ack = new Acknowledgement();
            expect(() => ack.refused).to.throw(AckError);
        });

        it('Should be true if its internal state is "REFUSED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.REFUSED;
            expect(ack.refused).to.be.true;
        });

        it('Should be false if its internal state is "CONFIRMED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.CONFIRMED;
            expect(ack.refused).to.be.false;
        });
    });

    describe('#confirm', () => {
        it('Should throw if the status is "CONFIRMED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.CONFIRMED;
            expect(() => ack.confirm()).to.throw(AckError);
        });
        it('Should throw if the status is "REFUSED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.REFUSED;
            expect(() => ack.confirm()).to.throw(AckError);
        });
        it('Should flip the instance state to "CONFIRMED"', () => {
            const ack = new Acknowledgement();
            ack.confirm();
            expect(ack.status).to.be.equal(Acknowledgement.CONFIRMED);
        });
    });

    describe('#refuse', () => {
        it('Should throw if the status is "CONFIRMED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.CONFIRMED;
            expect(() => ack.refuse()).to.throw(AckError);
        });
        it('Should throw if the status is "REFUSED"', () => {
            const ack = new Acknowledgement();
            ack.status = Acknowledgement.REFUSED;
            expect(() => ack.refuse()).to.throw(AckError);
        });
        it('Should flip the instance state to "REFUSED"', () => {
            const ack = new Acknowledgement();
            ack.refuse();
            expect(ack.status).to.be.equal(Acknowledgement.REFUSED);
        });
    });
});
