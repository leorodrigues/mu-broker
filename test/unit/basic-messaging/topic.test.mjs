import { expect, use } from 'chai';
import sinon from 'sinon';
import { Topic } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const fakeUrn = {
    isEquivalent: sandbox.stub(),
    resolves: sandbox.stub(),
    toURNString: sandbox.stub()
};

const fakeTopic = { urn: 112358 };

describe('Topic', () => {
    afterEach(() => sandbox.reset());

    describe('.urnString', function () {
        it('Should return the string representation of its urn', () => {
            fakeUrn.toURNString.returns('fake-result');
            const topic = new Topic(fakeUrn, 42);
            expect(topic.urnString).to.be.equal('fake-result');
        });
    });

    describe('#matches', function () {
        it('Should match if its urn "isEquivalent" and "resolves"', () => {
            fakeUrn.isEquivalent.returns(true);
            fakeUrn.resolves.returns(true);
            const topic = new Topic(fakeUrn, 132437);
            expect(topic.matches(fakeTopic)).to.be.true;
            expect(fakeUrn.isEquivalent).to.be.calledWithExactly(112358);
            expect(fakeUrn.resolves).to.be.calledWithExactly(132437);
        });
        it('Should fail to match if its urn fails an "isEquivalent" check', () => {
            fakeUrn.isEquivalent.returns(false);
            fakeUrn.resolves.returns(true);
            const topic = new Topic(fakeUrn, 132437);
            expect(topic.matches(fakeTopic)).to.be.false;
            expect(fakeUrn.isEquivalent).to.be.calledWithExactly(112358);
            expect(fakeUrn.resolves).not.be.called;
        });
        it('Should fail to match if its urn fails a "resolves" check', () => {
            fakeUrn.isEquivalent.returns(true);
            fakeUrn.resolves.returns(false);
            const topic = new Topic(fakeUrn, 132437);
            expect(topic.matches(fakeTopic)).to.be.false;
            expect(fakeUrn.isEquivalent).to.be.calledWithExactly(112358);
            expect(fakeUrn.resolves).to.be.calledWithExactly(132437);
        });
        it('Should fail to match if all checks fail', () => {
            fakeUrn.isEquivalent.returns(false);
            fakeUrn.resolves.returns(false);
            const topic = new Topic(fakeUrn, 132437);
            expect(topic.matches(fakeTopic)).to.be.false;
            expect(fakeUrn.isEquivalent).to.be.calledWithExactly(112358);
            expect(fakeUrn.resolves).to.not.be.called;
        });
    });
});