import { expect, use } from 'chai';
import sinon from 'sinon';
import { CountDownLatch } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

describe('CountDownLatch', function () {
    describe('#tick', function () {
        it('Should run the action after reaching zero', async () => {
            const subject = new CountDownLatch(3);

            const action = sandbox.stub();

            for (let i = 0; i < 4; i++)
                await subject.tick(action);

            expect(action).to.have.been.calledOnceWithExactly();
        });
    });
});