import { expect, use } from 'chai';
import { MuCommandBus } from '../../../index.mjs';
import sinon from 'sinon';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const muBroker = {
    publish: sandbox.stub(),
    subscribe: sandbox.stub()
};

const commandBrokeringKit = {
    newCommand: sandbox.stub()
};

describe('MuCommandBus', function () {

    afterEach(() => sandbox.reset());

    describe('#invokeFromResponse', function () {
        describe('Given no payload', function () {
            it('Should invoke the command w/o payload', async () => {
                const response = {
                    deriveCommand: sandbox.spy(() => ({ invocationId: 'x' }))
                };

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const outcome = await subject.invokeFromResponse(
                    'cmd-topic', 'rsp-topic', response, {
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(response.deriveCommand)
                    .to.have.been.calledOnceWithExactly(
                        commandBrokeringKit, 'rsp-topic', {
                            payload: undefined,
                            headers: { theMeaningOfLife: 42 }
                        }
                    );

                expect(outcome).to.be.equal('x');
            });
        });

        describe('Given a payload', function () {
            it('should invoke the command with the payload', async () => {
                const response = {
                    deriveCommand: sandbox.spy(() => ({ invocationId: 'y' }))
                };

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const outcome = await subject.invokeFromResponse(
                    'cmd-topic', 'rsp-topic', response, {
                        payload: 'fake-payload',
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(response.deriveCommand)
                    .to.have.been.calledOnceWithExactly(
                        commandBrokeringKit, 'rsp-topic', {
                            payload: 'fake-payload',
                            headers: { theMeaningOfLife: 42 }
                        }
                    );

                expect(outcome).to.be.equal('y');
            });
        });
    });

    describe('#invokeFromResponseAndForget', function () {
        describe('Given no payload', function () {
            it('Should invoke the command w/o payload', async () => {
                const response = { deriveFireAndForgetCommand: sandbox.stub() };

                response.deriveFireAndForgetCommand.returns({ invocationId: 'z' });

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const outcome = await subject.invokeFromResponseAndForget(
                    'cmd-topic', response, { headers: { theMeaningOfLife: 42 } });

                expect(response.deriveFireAndForgetCommand)
                    .to.have.been.calledOnceWithExactly(commandBrokeringKit, {
                        payload: undefined,
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(outcome).to.be.equal('z');
            });
        });
        describe('Given a payload', function () {
            it('Should invoke the command with the payload', async () => {
                const response = { deriveFireAndForgetCommand: sandbox.stub() };

                response.deriveFireAndForgetCommand.returns({ invocationId: 'w' });

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const outcome = await subject.invokeFromResponseAndForget(
                    'cmd-topic', response, {
                        payload: 'fake-payload',
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(response.deriveFireAndForgetCommand)
                    .to.have.been.calledOnceWithExactly(commandBrokeringKit, {
                        payload: 'fake-payload',
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(outcome).to.be.equal('w');
            });
        });
    });

    describe('#invokeFromCommand', function () {
        describe('Given no payload', function () {
            it('Should publish a command with no payload', async () => {
                const kit = commandBrokeringKit;
                const command = { deriveCommand: sandbox.stub() };

                command.deriveCommand.returns('fake-command');

                const subject = new MuCommandBus(muBroker, kit);
                await subject.invokeFromCommand(
                    'fake-cmd-topic', 'fake-rsp-topic', command, {
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(command.deriveCommand)
                    .to.have.been.calledOnceWithExactly(
                        kit, 'fake-rsp-topic', {
                            payload: undefined,
                            headers: { theMeaningOfLife: 42 }
                        });

                expect(muBroker.publish)
                    .to.have.been.calledOnceWithExactly(
                        'fake-cmd-topic', 'fake-command');
            });
        });

        describe('Given a payload', function () {
            it('Should publish a command with a payload', async () => {
                const kit = commandBrokeringKit;
                const command = { deriveCommand: sandbox.stub() };

                command.deriveCommand.returns('fake-command');

                const subject = new MuCommandBus(muBroker, kit);
                await subject.invokeFromCommand(
                    'fake-cmd-topic', 'fake-rsp-topic', command, {
                        payload: 'fake-payload',
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(command.deriveCommand)
                    .to.have.been.calledOnceWithExactly(
                        kit, 'fake-rsp-topic', {
                            payload: 'fake-payload',
                            headers: { theMeaningOfLife: 42 }
                        });

                expect(muBroker.publish)
                    .to.have.been.calledOnceWithExactly(
                        'fake-cmd-topic', 'fake-command');
            });
        });
    });

    describe('#invokeFromCommandAndForget', function () {
        describe('Given no payload', function () {
            it('Should publish a command with derived headers, and no payload', async () => {
                const kit = commandBrokeringKit;
                const command = { deriveFireAndForgetCommand: sandbox.stub() };

                command.deriveFireAndForgetCommand.returns('fake-command');

                const subject = new MuCommandBus(muBroker, kit);
                await subject.invokeFromCommandAndForget(
                    'fake-cmd-topic', command, {
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(command.deriveFireAndForgetCommand)
                    .to.have.been.calledOnceWithExactly(
                        kit, {
                            payload: undefined,
                            headers: { theMeaningOfLife: 42 }
                        });

                expect(muBroker.publish)
                    .to.have.been.calledOnceWithExactly(
                        'fake-cmd-topic', 'fake-command');
            });
        });

        describe('Given a payload', function () {
            it('Should publish a command with derived headers, and payload', async () => {
                const kit = commandBrokeringKit;
                const command = { deriveFireAndForgetCommand: sandbox.stub() };

                command.deriveFireAndForgetCommand.returns('fake-command');

                const subject = new MuCommandBus(muBroker, kit);
                await subject.invokeFromCommandAndForget(
                    'fake-cmd-topic', command, {
                        payload: 'fake-payload',
                        headers: { theMeaningOfLife: 42 }
                    });

                expect(command.deriveFireAndForgetCommand)
                    .to.have.been.calledOnceWithExactly(
                        kit, {
                            payload: 'fake-payload',
                            headers: { theMeaningOfLife: 42 }
                        });

                expect(muBroker.publish)
                    .to.have.been.calledOnceWithExactly(
                        'fake-cmd-topic', 'fake-command');
            });
        });
    });

    describe('#invoke', function () {
        describe('Given no payload', function () {
            it('Should publish a command carrying a response topic and w/o payload', async () => {
                commandBrokeringKit.newCommand.returns({ invocationId: 42 });

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const id = await subject.invoke('fake-cmd', 'fake-resp');

                expect(id).to.be.equal(42);

                expect(muBroker.publish)
                    .to.be.calledOnceWithExactly('fake-cmd', {
                        invocationId: 42
                    });

                expect(commandBrokeringKit.newCommand)
                    .to.be.calledOnceWithExactly({
                        headers: { responseTopic: 'fake-resp' },
                        payload: undefined
                    });
            });
        });

        describe('Given a payload', function () {
            it('Should publish a command carrying a response topic and w/ payload', async () => {
                commandBrokeringKit.newCommand.returns({ invocationId: 42 });

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const id = await subject.invoke('fake-cmd', 'fake-resp', {
                    payload: 'fake-payload'
                });

                expect(id).to.be.equal(42);

                expect(muBroker.publish)
                    .to.be.calledOnceWithExactly('fake-cmd', {
                        invocationId: 42
                    });

                expect(commandBrokeringKit.newCommand)
                    .to.be.calledOnceWithExactly({
                        headers: { responseTopic: 'fake-resp' },
                        payload: 'fake-payload'
                    });
            });
        });
    });

    describe('#invokeAndForget', function () {
        describe('Given no payload', function () {
            it('Should publish a command w/o payload', async () => {
                commandBrokeringKit.newCommand.returns({ invocationId: 42 });

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const id = await subject.invokeAndForget('fake-cmd', {
                    headers: { myValue: 19 }
                });

                expect(id).to.be.equal(42);

                expect(muBroker.publish)
                    .to.be.calledOnceWithExactly('fake-cmd', {
                        invocationId: 42
                    });

                expect(commandBrokeringKit.newCommand)
                    .to.be.calledOnceWithExactly({
                        headers: { myValue: 19 }, payload: undefined
                    });
            });
        });

        describe('Given a payload', function () {
            it('Should publish a command w/ payload', async () => {
                commandBrokeringKit.newCommand.returns({ invocationId: 42 });

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                const id = await subject.invokeAndForget('fake-cmd', {
                    headers: { myValue: 19 },
                    payload: 'fake-payload'
                });

                expect(id).to.be.equal(42);

                expect(muBroker.publish)
                    .to.be.calledOnceWithExactly('fake-cmd', {
                        invocationId: 42
                    });

                expect(commandBrokeringKit.newCommand)
                    .to.be.calledOnceWithExactly({
                        headers: { myValue: 19 },
                        payload: 'fake-payload'
                    });
            });
        });
    });

    describe('#respondTo', function () {
        describe('Given no payload', function () {
            it('Should derive a response from the given command and publish it', async () => {
                muBroker.publish.resolves();

                const command = {
                    withDerivedResponse: sandbox.spy(async (k, p, f) => {
                        return f('fake-response', 'fake-topic');
                    })
                };

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                await subject.respondTo(command, { headers: { myValue: 42 } });

                expect(command.withDerivedResponse)
                    .to.have.been.calledOnceWith(
                        commandBrokeringKit,
                        { headers: { myValue: 42 }, payload: undefined });

                expect(muBroker.publish)
                    .to.have.been.calledOnceWithExactly('fake-topic', 'fake-response');
            });
        });

        describe('Given a payload', function () {
            it('Should derive a response from the given command and publish it', async () => {
                muBroker.publish.resolves();

                const command = {
                    withDerivedResponse: sandbox.spy(async (k, p, f) => {
                        return f('fake-response', 'fake-topic');
                    })
                };

                const subject = new MuCommandBus(muBroker, commandBrokeringKit);

                await subject.respondTo(command, {
                    payload: 'fake-payload',
                    headers: { myValue: 42 }
                });

                expect(command.withDerivedResponse)
                    .to.have.been.calledOnceWith(commandBrokeringKit, {
                        payload: 'fake-payload',
                        headers: { myValue: 42 }
                    });

                expect(muBroker.publish)
                    .to.have.been.calledOnceWithExactly('fake-topic', 'fake-response');
            });
        });
    });

    describe('#subscribe', function () {
        it('Should delegate to the underlying broker.', async () => {
            muBroker.subscribe.resolves();

            const subject = new MuCommandBus(muBroker, commandBrokeringKit);

            await subject.subscribe('fake-topic', 'fake-receiver');

            expect(muBroker.subscribe)
                .to.have.been.calledOnceWithExactly('fake-topic', 'fake-receiver');
        });
    });

    describe('#publish', function () {
        it('Should delegate to the underlying broker.', async () => {
            muBroker.publish.resolves();

            const subject = new MuCommandBus(muBroker, commandBrokeringKit);

            await subject.publish('fake-topic', 'fake-payload');

            expect(muBroker.publish)
                .to.have.been.calledOnceWithExactly('fake-topic', 'fake-payload');
        });
    });
});