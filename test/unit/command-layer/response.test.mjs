import { expect, use  } from 'chai';
import sinon from 'sinon';
import { Response } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const kit = {
    newCommand: sandbox.stub()
};

describe('Response', function () {

    afterEach(() => sandbox.reset());

    describe('#invocationId', function () {
        describe('Given no previous invocation id', function () {
            it('Should return a surrogate invocation id', () => {
                const subject = new Response();
                expect(subject.invocationId).to.match(/^orphan:[a-z0-9]{16}$/);
            });
        });

        describe('Given a previous invocation id', function () {
            it('Should return the previous value', () => {
                const subject = new Response({ headers: { invocationId: 42 } });
                expect(subject.invocationId).to.be.equal(42);
            });
        });
    });

    describe('#deriveCommand', function () {
        it('Should derive a command with merged headers', () => {
            kit.newCommand.returns('fake-command');

            const subject = new Response({
                headers: {
                    myHeader: 42,
                    invocationId: 11,
                    correlationId: 19
                }
            });

            const command = subject.deriveCommand(kit, 'fake-topic', {
                headers: { theirHeader: 13 }
            });

            expect(command).to.be.equal('fake-command');

            expect(kit.newCommand).to.be.calledOnceWithExactly({
                payload: undefined,
                headers: {
                    myHeader: 42,
                    theirHeader: 13,
                    correlationId: 19,
                    responseTopic: 'fake-topic'
                }
            });
        });
    });

    describe('#deriveFireAndForgetCommand', function () {
        it('Should derive a command with merged headers', () => {
            kit.newCommand.returns('fake-command');

            const subject = new Response({
                headers: {
                    myHeader: 42,
                    invocationId: 11,
                    correlationId: 19
                }
            });

            const command = subject.deriveFireAndForgetCommand(kit, {
                headers: { theirHeader: 13 }
            });

            expect(command).to.be.equal('fake-command');

            expect(kit.newCommand).to.be.calledOnceWithExactly({
                payload: undefined,
                headers: {
                    myHeader: 42,
                    theirHeader: 13,
                    correlationId: 19
                }
            });
        });
    });
});