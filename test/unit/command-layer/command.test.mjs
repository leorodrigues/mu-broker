import { Command } from '../../../index.mjs';
import { expect, use } from 'chai';
import sinon from 'sinon';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const kit = {
    newResponse: sandbox.stub(),
    newCommand: sandbox.stub()
};

describe('Command', function () {

    afterEach(() => sandbox.reset());

    describe('#invocationId', function () {
        it('Should return the current invocation id', () => {
            const subject = new Command();
            expect(subject.invocationId).to.match(/^[a-z0-9]{16}$/);
        });
    });

    describe('#deriveCommand', function () {
        describe('Given no payload', function () {
            it('Should produce a new command with derived headers and no payload', () => {
                kit.newCommand.returns('fake-command-instance');

                const subject = new Command({
                    headers: { correlationId: 11, invocationId: 13 }
                });

                const outcome = subject.deriveCommand(kit, 'fake-topic', {
                    headers: { theMeaningOfLife: 42 }
                });

                expect(outcome).to.be.equal('fake-command-instance');
                expect(kit.newCommand).to.have.been.calledOnceWithExactly({
                    payload: undefined,
                    headers: {
                        theMeaningOfLife: 42,
                        correlationId: 11,
                        invocationId: 13,
                        responseTopic: 'fake-topic'
                    }
                });
            });
        });

        describe('Given a payload', function () {
            it('Should produce a new command with derived headers and a payload', () => {
                kit.newCommand.returns('fake-command-instance');

                const subject = new Command({
                    headers: { correlationId: 11, invocationId: 13 }
                });

                const outcome = subject.deriveCommand(kit, 'fake-topic', {
                    headers: { theMeaningOfLife: 42 },
                    payload: 'fake-payload'
                });

                expect(outcome).to.be.equal('fake-command-instance');
                expect(kit.newCommand).to.have.been.calledOnceWithExactly({
                    payload: 'fake-payload',
                    headers: {
                        theMeaningOfLife: 42,
                        correlationId: 11,
                        invocationId: 13,
                        responseTopic: 'fake-topic'
                    }
                });
            });
        });
    });

    describe('#deriveFireAndForgetCommand', function () {
        describe('Given no payload', function () {
            it('Should produce a new command with derived headers and no payload', () => {
                kit.newCommand.returns('fake-command-instance');

                const subject = new Command({
                    headers: { correlationId: 17, invocationId: 19 }
                });

                const outcome = subject.deriveFireAndForgetCommand(kit, {
                    headers: { theMeaningOfLife: 42 }
                });

                expect(outcome).to.be.equal('fake-command-instance');
                expect(kit.newCommand).to.have.been.calledOnceWithExactly({
                    payload: undefined,
                    headers: {
                        theMeaningOfLife: 42,
                        correlationId: 17,
                        invocationId: 19
                    }
                });
            });
        });

        describe('Given a payload', function () {
            it('Should produce a new command with derived headers and a payload', () => {
                kit.newCommand.returns('fake-command-instance');

                const subject = new Command({
                    headers: { correlationId: 17, invocationId: 19 }
                });

                const outcome = subject.deriveFireAndForgetCommand(kit, {
                    payload: 'fake-payload',
                    headers: { theMeaningOfLife: 42 }
                });

                expect(outcome).to.be.equal('fake-command-instance');
                expect(kit.newCommand).to.have.been.calledOnceWithExactly({
                    payload: 'fake-payload',
                    headers: {
                        theMeaningOfLife: 42,
                        correlationId: 17,
                        invocationId: 19
                    }
                });
            });
        });
    });

    describe('#withDerivedResponse', function () {
        describe('Given there is no response topic header', function () {
            it('Should skip execution', () => {
                const subject = new Command();
                const action = sandbox.stub();

                subject.withDerivedResponse(kit, { }, action);

                expect(action).to.not.have.been.called;
                expect(kit.newResponse).to.not.have.been.called;
            });
        });

        describe('Given there is a response topic header', function () {
            it('Should derive a response and invoke the action', () => {
                kit.newResponse.returns('fake-response');

                const subject = new Command({
                    headers: {
                        correlationId: 42,
                        responseTopic: 'fake-topic',
                        invocationId: 'test-invocation'
                    }
                });

                const action = sandbox.stub();

                const headers = { myHeader: 112358 };
                subject.withDerivedResponse(kit, { headers }, action);

                expect(kit.newResponse).to.have.been.calledOnceWithExactly({
                    payload: undefined,
                    headers: {
                        myHeader: 112358,
                        correlationId: 42,
                        invocationId: 'test-invocation'
                    }
                });

                expect(action).to.have.been.calledOnceWithExactly(
                    'fake-response', 'fake-topic');
            });
        });
    });
});