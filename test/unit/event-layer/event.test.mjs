import { expect, use } from 'chai';
import sinon from 'sinon';
import { Event } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const eventBrokeringKit = {
    newEvent: sandbox.stub(),
    newCommand: sandbox.stub()
};

describe('Event', function () {

    afterEach(() => sandbox.reset());

    describe('#deriveEvent', function () {
        describe('Given no payload', function () {
            it('Should create a new event with derived headers, and w/o payload', () => {
                eventBrokeringKit.newEvent.returns('fake-event');

                const subject = new Event({
                    headers: { theMeaningOfLife: 42, correlationId: 2 },
                    payload: 'old-payload'
                });

                const outcome = subject.deriveEvent(eventBrokeringKit, {
                    headers: { pi: 3.14 }
                });

                expect(outcome).to.be.equal('fake-event');

                expect(eventBrokeringKit.newEvent)
                    .to.have.been.calledOnceWithExactly({
                        payload: undefined,
                        headers: {
                            theMeaningOfLife: 42,
                            correlationId: 2,
                            pi: 3.14
                        }
                    });
            });
        });

        describe('Given a payload', function () {
            it('Should create a new event with derived headers, and w/ payload', () => {
                eventBrokeringKit.newEvent.returns('fake-event');

                const subject = new Event({
                    headers: { theMeaningOfLife: 42, correlationId: 3 },
                    payload: 'old-payload'
                });

                const outcome = subject.deriveEvent(eventBrokeringKit, {
                    headers: { pi: 3.14 },
                    payload: 'new-payload'
                });

                expect(outcome).to.be.equal('fake-event');

                expect(eventBrokeringKit.newEvent)
                    .to.have.been.calledOnceWithExactly({
                        payload: 'new-payload',
                        headers: {
                            theMeaningOfLife: 42,
                            correlationId: 3,
                            pi: 3.14
                        }
                    });
            });
        });
    });

    describe('#deriveCommand', function () {
        describe('Given no payload', function () {
            it('Should create a new command with derived headers, and w/o payload', () => {
                eventBrokeringKit.newCommand.returns('fake-command');

                const subject = new Event({
                    headers: { theMeaningOfLife: 42, correlationId: 5 },
                    payload: 'old-payload'
                });

                const outcome = subject.deriveCommand(
                    eventBrokeringKit,
                    'x',
                    {
                        headers: { pi: 3.14 }
                    });

                expect(outcome).to.be.equal('fake-command');

                expect(eventBrokeringKit.newCommand)
                    .to.have.been.calledOnceWithExactly({
                        payload: undefined,
                        headers: {
                            responseTopic: 'x',
                            theMeaningOfLife: 42,
                            correlationId: 5,
                            pi: 3.14
                        }
                    });
            });
        });

        describe('Given a payload', function () {
            it('Should create a new command with derived headers, and w/ payload', () => {
                eventBrokeringKit.newCommand.returns('fake-command');

                const subject = new Event({
                    headers: { theMeaningOfLife: 42, correlationId: 7 },
                    payload: 'old-payload'
                });

                const outcome = subject.deriveCommand(eventBrokeringKit, 'y', {
                    headers: { pi: 3.14 },
                    payload: 'new-payload'
                });

                expect(outcome).to.be.equal('fake-command');

                expect(eventBrokeringKit.newCommand)
                    .to.have.been.calledOnceWithExactly({
                        payload: 'new-payload',
                        headers: {
                            responseTopic: 'y',
                            theMeaningOfLife: 42,
                            correlationId: 7,
                            pi: 3.14
                        }
                    });
            });
        });
    });

    describe('#deriveFireAndForgetCommand', function () {
        describe('Given no payload', function () {
            it('Should produce a command without payload', () => {
                eventBrokeringKit.newCommand.returns('fake-command');

                const subject = new Event({
                    headers: { theMeaningOfLife: 42, correlationId: 5 },
                    payload: 'old-payload'
                });

                const outcome = subject.deriveFireAndForgetCommand(
                    eventBrokeringKit, { headers: { pi: 3.14 } });

                expect(outcome).to.be.equal('fake-command');

                expect(eventBrokeringKit.newCommand)
                    .to.have.been.calledOnceWithExactly({
                        payload: undefined,
                        headers: {
                            theMeaningOfLife: 42,
                            correlationId: 5,
                            pi: 3.14
                        }
                    });
            });
        });

        describe('Given a payload', function () {
            it('Should produce a command with the payload', () => {
                eventBrokeringKit.newCommand.returns('fake-command');

                const subject = new Event({
                    headers: { theMeaningOfLife: 42, correlationId: 5 },
                    payload: 'old-payload'
                });

                const outcome = subject.deriveFireAndForgetCommand(
                    eventBrokeringKit, {
                        headers: { pi: 3.14 },
                        payload: 'new-payload'
                    });

                expect(outcome).to.be.equal('fake-command');

                expect(eventBrokeringKit.newCommand)
                    .to.have.been.calledOnceWithExactly({
                        payload: 'new-payload',
                        headers: {
                            theMeaningOfLife: 42,
                            correlationId: 5,
                            pi: 3.14
                        }
                    });
            });
        });
    });
});