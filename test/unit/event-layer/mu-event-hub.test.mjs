import { expect, use } from 'chai';
import sinon from 'sinon';
import { MuEventHub } from '../../../index.mjs';

use((await import('sinon-chai')).default);

const sandbox = sinon.createSandbox();

const muCommandBus = {
    invoke: sandbox.stub(),
    invokeAndForget: sandbox.stub(),
    invokeFromCommand: sandbox.stub(),
    invokeFromCommandAndForget: sandbox.stub(),
    invokeFromResponse: sandbox.stub(),
    invokeFromResponseAndForget: sandbox.stub(),
    respondTo: sandbox.stub(),
    subscribe: sandbox.stub(),
    publish: sandbox.stub()
};

const eventBrokeringKit = {
    newEvent: sandbox.stub()
};

describe('MuEventHub', function () {

    afterEach(() => sandbox.reset());

    describe('#triggerFromEvent', function () {
        describe('Given no payload', function () {
            it('Should trigger one event from another w/o payload', async () => {
                muCommandBus.publish.resolves();

                const event = { deriveEvent: sandbox.spy(() => 'evt') };

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.triggerFromEvent('e-topic', event, { headers: 'h' });

                expect(event.deriveEvent)
                    .to.have.been.calledOnceWithExactly(
                        eventBrokeringKit, { headers: 'h', payload: undefined });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('e-topic', 'evt');
            });
        });

        describe('Given a payload', function () {
            it('Should trigger one event from another w/ the payload', async () => {
                muCommandBus.publish.resolves();

                const event = { deriveEvent: sandbox.spy(() => 'evt') };

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.triggerFromEvent(
                    'e-topic', event, { headers: 'h', payload: 'p' });

                expect(event.deriveEvent)
                    .to.have.been.calledOnceWithExactly(
                        eventBrokeringKit, { headers: 'h', payload: 'p' });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('e-topic', 'evt');
            });
        });
    });

    describe('#triggerFromCommand', function () {
        describe('Given no payload', function () {
            it('Should derive headers, and fire an event w/o the payload', async () => {
                const command = { deriveHeaders: sandbox.stub() };

                command.deriveHeaders.returns('fake-headers');
                eventBrokeringKit.newEvent.returns('fake-event');

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.triggerFromCommand('fake-topic', command, {
                    headers: { theMeaningOfLife: 42 }
                });

                expect(command.deriveHeaders)
                    .to.have.been.calledOnceWithExactly({ theMeaningOfLife: 42 });

                expect(eventBrokeringKit.newEvent)
                    .to.have.been.calledOnceWithExactly({
                        headers: 'fake-headers', payload: undefined
                    });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('fake-topic', 'fake-event');
            });
        });

        describe('Given a payload', function () {
            it('Should derive headers, and fire an event w/ the payload', async () => {
                const command = { deriveHeaders: sandbox.stub() };

                command.deriveHeaders.returns('fake-headers');
                eventBrokeringKit.newEvent.returns('fake-event');

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.triggerFromCommand('fake-topic', command, {
                    headers: { theMeaningOfLife: 42 },
                    payload: 'fake-payload'
                });

                expect(command.deriveHeaders)
                    .to.have.been.calledOnceWithExactly({ theMeaningOfLife: 42 });

                expect(eventBrokeringKit.newEvent)
                    .to.have.been.calledOnceWithExactly({
                        headers: 'fake-headers', payload: 'fake-payload'
                    });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('fake-topic', 'fake-event');
            });
        });
    });

    describe('#trigger', function () {
        describe('Given no payload', function () {
            it('Should publish only the headers', async () => {
                muCommandBus.publish.resolves();
                eventBrokeringKit.newEvent.returns('fake-event');

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.trigger('fake-topic', {
                    headers: { theMeaningOfLife: 42 }
                });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly(
                        'fake-topic', 'fake-event');

                expect(eventBrokeringKit.newEvent)
                    .to.be.have.been.calledOnceWithExactly({
                        headers: { theMeaningOfLife: 42 },
                        payload: undefined
                    });
            });
        });

        describe('Given a payload', function () {
            it('Should publish the headers and the payload', async () => {
                muCommandBus.publish.resolves();
                eventBrokeringKit.newEvent.returns('fake-event');

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.trigger('fake-topic', {
                    headers: { theMeaningOfLife: 42 },
                    payload: 'fake-payload'
                });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly(
                        'fake-topic', 'fake-event');

                expect(eventBrokeringKit.newEvent)
                    .to.be.have.been.calledOnceWithExactly({
                        headers: { theMeaningOfLife: 42 },
                        payload: 'fake-payload'
                    });
            });
        });
    });

    describe('#invoke', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.invoke.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.invoke('fake-cmd', 'fake-resp', {
                headers: 'fake-headers',
                payload: 'fake-payload'
            });

            expect(muCommandBus.invoke)
                .to.have.been.calledOnceWithExactly('fake-cmd', 'fake-resp', {
                    headers: 'fake-headers',
                    payload: 'fake-payload'
                });
        });
    });

    describe('#invokeAndForget', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.invokeAndForget.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.invokeAndForget('fake-cmd', {
                headers: 'fake-headers',
                payload: 'fake-payload'
            });

            expect(muCommandBus.invokeAndForget)
                .to.have.been.calledOnceWithExactly('fake-cmd', {
                    headers: 'fake-headers',
                    payload: 'fake-payload'
                });
        });
    });

    describe('#invokeFromCommand', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.invokeFromCommand.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.invokeFromCommand(
                'cmd-topic', 'rsp-topic', 'cmd', { payload: 'x', headers: 'y' });

            expect(muCommandBus.invokeFromCommand)
                .to.have.been.calledOnceWithExactly(
                    'cmd-topic', 'rsp-topic', 'cmd', { payload: 'x', headers: 'y' });
        });
    });

    describe('#invokeFromCommandAndForget', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.invokeFromCommandAndForget.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.invokeFromCommandAndForget(
                'cmd-topic', 'cmd', { payload: 'x', headers: 'y' });

            expect(muCommandBus.invokeFromCommandAndForget)
                .to.have.been.calledOnceWithExactly(
                    'cmd-topic', 'cmd', { payload: 'x', headers: 'y' });
        });
    });

    describe('#invokeFromResponse', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.invokeFromResponse.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.invokeFromResponse(
                'cmd-topic', 'rsp-topic', 'rsp', { payload: 'x', headers: 'y' });

            expect(muCommandBus.invokeFromResponse)
                .to.have.been.calledOnceWithExactly(
                    'cmd-topic', 'rsp-topic', 'rsp', { payload: 'x', headers: 'y' });
        });
    });

    describe('#invokeFromResponseAndForget', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.invokeFromResponseAndForget.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.invokeFromResponseAndForget(
                'cmd-topic', 'rsp', { payload: 'x', headers: 'y' });

            expect(muCommandBus.invokeFromResponseAndForget)
                .to.have.been.calledOnceWithExactly(
                    'cmd-topic', 'rsp', { payload: 'x', headers: 'y' });
        });
    });

    describe('#invokeFromEvent', function () {
        describe('Given no payload', function () {
            it('Should invoke a command from an event w/o payload', async () => {
                muCommandBus.publish.resolves();

                const event = { deriveCommand: sandbox.spy(() => 'cmd') };

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.invokeFromEvent(
                    'e-topic', 'r-topic', event, { headers: 'h' });

                expect(event.deriveCommand)
                    .to.have.been.calledOnceWithExactly(
                        eventBrokeringKit, 'r-topic', { payload: undefined, headers: 'h' });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('e-topic', 'cmd');
            });
        });

        describe('Given a payload', function () {
            it('Should invoke a command from an event with the payload', async () => {
                muCommandBus.publish.resolves();

                const event = { deriveCommand: sandbox.spy(() => 'cmd') };

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.invokeFromEvent(
                    'e-topic', 'r-topic', event, { headers: 'h', payload: 'p' });

                expect(event.deriveCommand)
                    .to.have.been.calledOnceWithExactly(
                        eventBrokeringKit, 'r-topic', { headers: 'h', payload: 'p' });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('e-topic', 'cmd');
            });
        });
    });

    describe('#invokeFromEventAndForget', function () {
        describe('Given no payload', function () {
            it('Should invoke a command from an event w/o payload', async () => {
                muCommandBus.publish.resolves();

                const event = { deriveFireAndForgetCommand: sandbox.spy(() => 'cmd') };

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.invokeFromEventAndForget('e-topic', event, { headers: 'h' });

                expect(event.deriveFireAndForgetCommand)
                    .to.have.been.calledOnceWithExactly(
                        eventBrokeringKit, { headers: 'h', payload: undefined });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('e-topic', 'cmd');
            });
        });

        describe('Given a payload', function () {
            it('Should invoke a command from an event with the payload', async () => {
                muCommandBus.publish.resolves();

                const event = { deriveFireAndForgetCommand: sandbox.spy(() => 'cmd') };

                const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

                await subject.invokeFromEventAndForget(
                    'e-topic', event, { headers: 'h', payload: 'p' });

                expect(event.deriveFireAndForgetCommand)
                    .to.have.been.calledOnceWithExactly(
                        eventBrokeringKit, { headers: 'h', payload: 'p' });

                expect(muCommandBus.publish)
                    .to.have.been.calledOnceWithExactly('e-topic', 'cmd');
            });
        });
    });

    describe('#respondTo', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.respondTo.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.respondTo('fake-cmd', {
                headers: 'fake-headers',
                payload: 'fake-payload'
            });

            expect(muCommandBus.respondTo)
                .to.have.been.calledOnceWithExactly('fake-cmd', {
                    headers: 'fake-headers',
                    payload: 'fake-payload'
                });
        });
    });

    describe('#subscribe', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.subscribe.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.subscribe('fake-topic', 'fake-receiver');

            expect(muCommandBus.subscribe)
                .to.have.been.calledOnceWithExactly('fake-topic', 'fake-receiver');
        });
    });

    describe('#publish', function () {
        it('Should delegate to the command bus', async () => {
            muCommandBus.subscribe.resolves();

            const subject = new MuEventHub(muCommandBus, eventBrokeringKit);

            await subject.publish('fake-topic', 'fake-payload');

            expect(muCommandBus.publish)
                .to.have.been.calledOnceWithExactly('fake-topic', 'fake-payload');
        });
    });
});