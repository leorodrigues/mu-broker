import { expect } from 'chai';

import {
    CommandBrokeringKit,
    BrokeringKit,
    CountDownLatch
} from '../../index.mjs';

import makeLogger from '@leorodrigues/logging';

const logger = makeLogger('brokering-experiment');

const brokeringKit = new CommandBrokeringKit(new BrokeringKit(logger));

describe('command-layer', function () {
    describe('With a MuCommandBus', function () {
        it('Should be possible to invoke commands from other commands', async () => {
            const countDown = new CountDownLatch(4);

            const firstCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first');

            const firstResponseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first/response');

            const secondCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#second');

            const secondResponseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#second/response');

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const bus = brokeringKit.newMuCommandBus(broker);

                bus.subscribe(firstCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command',
                            'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'beta' });

                        bus.invokeFromCommand(
                            secondCommandTopic,
                            secondResponseTopic,
                            command,
                            { payload: 'gamma' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(firstResponseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('Handling response',
                            'object', JSON.stringify(response));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(secondCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command',
                            'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'delta' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(secondResponseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('Handling response',
                            'object', JSON.stringify(response));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.invoke(
                    firstCommandTopic,
                    firstResponseTopic,
                    { payload: 'alpha' });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to invoke commands from other commands and forget', async () => {
            const countDown = new CountDownLatch(2);

            const firstCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first');

            const secondCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#second');

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const bus = brokeringKit.newMuCommandBus(broker);

                bus.subscribe(firstCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command',
                            'object', JSON.stringify(command));

                        bus.invokeFromCommandAndForget(
                            secondCommandTopic,
                            command,
                            { payload: 'gamma' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(secondCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command',
                            'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'gamma' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.invokeAndForget(firstCommandTopic, { payload: 'alpha' });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to invoke commands from responses', async () => {
            const countDown = new CountDownLatch(4);

            const firstCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first');

            const firstResponseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first/response');

            const secondCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#second');

            const secondResponseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#second/response');

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const bus = brokeringKit.newMuCommandBus(broker);

                bus.subscribe(firstCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command', 'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'first response' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(firstResponseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('Handling response', 'object', JSON.stringify(response));

                        bus.invokeFromResponse(
                            secondCommandTopic,
                            secondResponseTopic,
                            response, {
                                payload: 'second invocation'
                            });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(secondCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command', 'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'second response' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(secondResponseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('Handling response', 'object', JSON.stringify(response));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.invoke(
                    firstCommandTopic,
                    firstResponseTopic,
                    { payload: 'first invocation' });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to invoke commands from responses and forget', async () => {
            const countDown = new CountDownLatch(3);

            const firstCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first');

            const firstResponseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first/response');

            const secondCommandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#second');

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const bus = brokeringKit.newMuCommandBus(broker);

                bus.subscribe(firstCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command', 'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'two' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(firstResponseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('Handling response', 'object', JSON.stringify(response));

                        bus.invokeFromResponseAndForget(
                            secondCommandTopic,
                            response,
                            { payload: 'three' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(secondCommandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command', 'object', JSON.stringify(command));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.invoke(
                    firstCommandTopic,
                    firstResponseTopic,
                    { payload: 'one' });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to invoke commands and forget about the response.', async () => {
            const countDown = new CountDownLatch(1);

            const commandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:command');

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const bus = brokeringKit.newMuCommandBus(broker);

                bus.subscribe(commandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command.', 'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'hello jack' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.invokeAndForget(commandTopic, { payload: 'hello world' });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to invoke commands and handle responses.', async () => {
            const countDown = new CountDownLatch(2);

            const commandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:command');

            const responseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:response');

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const bus = brokeringKit.newMuCommandBus(broker);

                bus.subscribe(commandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('Handling command', 'object', JSON.stringify(command));

                        bus.respondTo(command, { payload: 'hello jack' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.subscribe(responseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('Handling response', 'object', JSON.stringify(response));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                bus.invoke(commandTopic, responseTopic, { payload: 'hello world' });
            });

            expect(countDown.done).to.be.true;
        });
    });
});