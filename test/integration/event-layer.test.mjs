import { expect } from 'chai';

import {
    EventBrokeringKit,
    CommandBrokeringKit,
    BrokeringKit,
    CountDownLatch
} from '../../index.mjs';

import makeLogger from '@leorodrigues/logging';

const logger = makeLogger('brokering-experiment');

const brokeringKit = new EventBrokeringKit(
    new CommandBrokeringKit(
        new BrokeringKit(logger)
    )
);

describe('event-layer', function () {
    describe('With a MuEventHub', function () {
        it('Should be possible to invoke commands from events', async () => {
            const countDown = new CountDownLatch(3);

            const commandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first');

            const responseTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first/response');

            const eventTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:events#it-happened');

            const muBroker = brokeringKit.newMuBroker();
            const muCommandBus = brokeringKit.newMuCommandBus(muBroker);
            const hub = brokeringKit.newEventHub(muCommandBus);

            await new Promise(resolve => {
                hub.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'object', JSON.stringify(event));

                        hub.invokeFromEvent(commandTopic, responseTopic, event, {
                            payload: 'second'
                        });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.subscribe(commandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('handling command.',
                            'object', JSON.stringify(command));

                        hub.respondTo(command, { payload: 'third' });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.subscribe(responseTopic, {
                    handle(response, acknowledgement) {
                        logger.info('handling response.',
                            'object', JSON.stringify(response));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.trigger(eventTopic, {
                    payload: 'first',
                    headers: { theMeaningOfLife: 42 }
                });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to invoke fire-and-forget commands from events', async () => {
            const countDown = new CountDownLatch(2);

            const eventTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:events#it-happened');

            const commandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:commands#first');

            const muBroker = brokeringKit.newMuBroker();
            const muCommandBus = brokeringKit.newMuCommandBus(muBroker);
            const hub = brokeringKit.newEventHub(muCommandBus);

            await new Promise(resolve => {
                hub.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'object', JSON.stringify(event));

                        hub.invokeFromEventAndForget(commandTopic, event, {
                            payload: 'second'
                        });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.subscribe(commandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('handling command.',
                            'object', JSON.stringify(command));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.trigger(eventTopic, {
                    payload: 'first',
                    headers: { theMeaningOfLife: 42 }
                });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to trigger events from events', async () => {
            const countDown = new CountDownLatch(2);

            const firstEventTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:events#carrier-has-arrived');

            const secondEventTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:events#teleport-successful');

            const muBroker = brokeringKit.newMuBroker();
            const muCommandBus = brokeringKit.newMuCommandBus(muBroker);
            const hub = brokeringKit.newEventHub(muCommandBus);

            await new Promise(resolve => {
                hub.subscribe(firstEventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'object', JSON.stringify(event));

                        hub.triggerFromEvent(secondEventTopic, event, {
                            payload: 'second'
                        });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.subscribe(secondEventTopic, {
                    handle(command, acknowledgement) {
                        logger.info('handling command.',
                            'object', JSON.stringify(command));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                hub.trigger(firstEventTopic, {
                    payload: 'first',
                    headers: { theMeaningOfLife: 42 }
                });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to trigger events from commands', async () => {
            const countDown = new CountDownLatch(4);

            const commandTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:command');

            const eventTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:event');

            const muBroker = brokeringKit.newMuBroker();
            const muCommandBus = brokeringKit.newMuCommandBus(muBroker);
            const subject = brokeringKit.newEventHub(muCommandBus);

            await new Promise(resolve => {
                subject.subscribe(commandTopic, {
                    handle(command, acknowledgement) {
                        logger.info('handling command.',
                            'object', JSON.stringify(command));

                        subject.triggerFromCommand(eventTopic, command, {
                            payload: 'fake command handled'
                        });

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'consumerId', 1, 'object', JSON.stringify(event));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'consumerId', 2, 'object', JSON.stringify(event));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'consumerId', 3, 'object', JSON.stringify(event));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.invokeAndForget(commandTopic, {
                    payload: 'hello world',
                    headers: { theMeaningOfLife: 42 }
                });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should be possible to trigger independent events', async () => {
            const countDown = new CountDownLatch(3);

            const eventTopic = brokeringKit
                .newTopic('urn:leo-org:mu-broker:experiment:event');

            const muBroker = brokeringKit.newMuBroker();
            const muCommandBus = brokeringKit.newMuCommandBus(muBroker);
            const subject = brokeringKit.newEventHub(muCommandBus);

            await new Promise(resolve => {
                subject.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'consumerId', 1, 'object', JSON.stringify(event));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'consumerId', 2, 'object', JSON.stringify(event));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.subscribe(eventTopic, {
                    handle(event, acknowledgement) {
                        logger.info('handling event.',
                            'consumerId', 3, 'object', JSON.stringify(event));

                        acknowledgement.confirm();

                        return countDown.tick(resolve);
                    }
                });

                subject.trigger(eventTopic, {
                    payload: 'hello world',
                    headers: { theMeaningOfLife: 42 }
                });
            });

            expect(countDown.done).to.be.true;
        });
    });
});