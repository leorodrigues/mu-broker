import { expect } from 'chai';
import { BrokeringKit, CountDownLatch } from '../../index.mjs';
import makeLogger from '@leorodrigues/logging';

const logger = makeLogger('brokering-experiment');

const brokeringKit = new BrokeringKit(logger);

describe('basic-messaging', function () {
    describe('Given a MuBroker instance', function () {
        it('Should send and receive simple messages', async () => {
            const countDown = new CountDownLatch(2);

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const echoTopic = brokeringKit.newTopic('urn:leo-org:mu-broker:experiment:event');

                broker.subscribe(echoTopic, {
                    handle(evt, acknowledgement) {
                        logger.info('As JSON', 'json', JSON.stringify(evt));
                        acknowledgement.confirm();
                        return countDown.tick(resolve);
                    }
                });

                broker.subscribe(echoTopic, {
                    handle(evt, acknowledgement) {
                        logger.info('As text', 'text', evt.text);
                        acknowledgement.confirm();
                        return countDown.tick(resolve);
                    }
                });

                broker.publish(echoTopic, { text: 'hello world' });
            });

            expect(countDown.done).to.be.true;
        });

        it('Should send and receive envelopes', async () => {
            const countDown = new CountDownLatch(2);

            await new Promise(resolve => {
                const broker = brokeringKit.newMuBroker();
                const echoTopic = brokeringKit.newTopic('urn:leo-org:mu-broker:experiment:event');

                broker.subscribe(echoTopic, {
                    handle(envelope, acknowledgement) {
                        logger.info('Whole object', 'json', JSON.stringify(envelope));
                        acknowledgement.confirm();
                        return countDown.tick(resolve);
                    }
                });

                broker.subscribe(echoTopic, {
                    handle(envelope, acknowledgement) {
                        logger.info('Payload', 'content', envelope.payload);
                        acknowledgement.confirm();
                        return countDown.tick(resolve);
                    }
                });

                broker.publish(echoTopic, brokeringKit.newEnvelope({
                    payload: 'hello world',
                    headers: { theMeaningOfLife: 42 }
                }));
            });

            expect(countDown.done).to.be.true;
        });
    });
});