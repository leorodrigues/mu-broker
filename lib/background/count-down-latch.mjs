

export class CountDownLatch {
    constructor(count = 1) {
        this.current = count;
        this.count = count;
    }

    get done() {
        return this.current < 1;
    }

    reset(count) {
        this.current = parseInt(count || this.count);
    }

    async tick(action = async () => { }, ...args) {
        if (this.current > -1)
            this.current--;
        if (this.current === 0)
            return await action(...args);
    }
}