
import { URNParser } from '@leorodrigues/urn-toolkit';
import Trampoline from '@leorodrigues/trampoline';

import { Topic } from './topic.mjs';
import { MessageQueue } from './message-queue.mjs';
import { Subscriber } from './subscriber.mjs';
import { ConsumeQueueAction } from './consume-queue-action.mjs';
import { MessageQueueArray } from './message-queue-array.mjs';
import { Broadcaster } from './broadcaster.mjs';
import { MuBroker } from './mu-broker.mjs';
import { Envelope } from './envelope.mjs';

/* istanbul ignore next */
export class BrokeringKit {
    constructor(logger, urnParser) {
        this.urnParser = urnParser || new URNParser();
        this.logger = logger;
    }

    newTopic(urnString, data) {
        return new Topic(this.urnParser.parseURNString(urnString), data);
    }

    newTopicFromURN(urn, data) {
        return new Topic(urn, data);
    }

    newMessageQueue(topic) {
        return new MessageQueue(topic);
    }

    newSubscriber(topic, receiver, messageQueue) {
        return new Subscriber(topic, new Trampoline(
            new ConsumeQueueAction(receiver, messageQueue, this.logger)));
    }

    newMuBroker() {
        const messageQueueArray = new MessageQueueArray();
        const broadcaster = new Broadcaster();
        return new MuBroker(broadcaster, messageQueueArray, this);
    }

    newEnvelope({ headers = { }, payload } = { }) {
        return new Envelope({ payload, headers });
    }
}
