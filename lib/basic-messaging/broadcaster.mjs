import { SubscriberArray } from './subscriber-array.mjs';

export class Broadcaster {
    constructor(subscribers) {
        /* istanbul ignore next */
        this.subscribers = subscribers || new SubscriberArray();
    }

    push(subscriber) {
        this.subscribers.push(subscriber);
    }

    broadcast(topic) {
        for (const s of this.subscribers.findAllListeningTo(topic))
            s.wakeUp();
    }
}
