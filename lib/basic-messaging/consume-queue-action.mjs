import { Acknowledgement } from './acknowledgement.mjs';

export class ConsumeQueueAction {
    constructor(receiver, messageQueue, logger) {
        Object.assign(this, { receiver, messageQueue, logger });
    }

    async fire() {
        if ((this.messageQueue.length || 0) === 0)
            return true;

        const acknowledgement = new Acknowledgement();
        const currentMessage = this.messageQueue.shift();
        try {
            await this.receiver.handle(currentMessage, acknowledgement);
            return false;
        } finally {
            if (acknowledgement.refused)
                this.messageQueue.push(currentMessage);
        }
    }

    /* istanbul ignore next */
    async handleError(error) {
        this.logger.error(error.message);
        return false;
    }
}
