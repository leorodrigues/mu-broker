export class MessageQueue extends Array {
    constructor(topic) {
        super();
        this.topic = topic;
    }

    acceptsTopic(topic) {
        return this.topic.matches(topic);
    }
}
