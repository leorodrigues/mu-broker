import { randomBytes } from 'node:crypto';

const excluding = blackList => k => !blackList.includes(k);

export class Envelope {
    constructor({ headers = { }, payload } = { }) {
        this.attributes = { headers };

        if (payload)
            this.attributes.payload = payload;

        this.ensureHeader('correlationId');
    }

    get isEmpty() {
        return isMissing(this.attributes.payload);
    }

    get correlationId() {
        return this.headers.correlationId;
    }

    get headers() {
        return this.attributes.headers || { };
    }

    get payload() {
        return this.attributes.payload;
    }

    deriveEnvelope(brokeringKit, { headers = { }, payload } = { }) {
        const derivedHeaders = this.deriveHeaders(headers);

        if (isMissing(payload))
            return brokeringKit.newEnvelope({ headers: derivedHeaders });

        return brokeringKit.newEnvelope({ headers: derivedHeaders, payload });
    }

    deriveHeaders(headers, attributesToExclude = [ ]) {
        const myHeaders = this.headers;

        attributesToExclude = [ 'correlationId', ...attributesToExclude ];

        const selectedAttributes = Object.keys(myHeaders)
            .filter(excluding(attributesToExclude));

        selectedAttributes.push(...Object.keys(headers)
            .filter(excluding(attributesToExclude))
            .filter(unique));

        const selectedHeaders = { };
        for (const a of selectedAttributes)
            selectedHeaders[a] = headers[a] || myHeaders[a];

        selectedHeaders.correlationId = myHeaders.correlationId;

        return selectedHeaders;
    }

    ensureHeader(name, computeValue = random8Bytes) {
        const headers = this.attributes.headers;

        if (isMissing(headers[name]))
            headers[name] = computeValue();

        return headers;
    }
}

function random8Bytes() {
    return randomBytes(8).toString('hex');
}

function isMissing(value) {
    return value === undefined
        || value === null
        || (typeof value === 'string' && value.trim() === '');
}

function unique(value, index, array) {
    return array.findIndex(v => v === value) === index;
}
