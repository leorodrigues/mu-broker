export class Topic {
    constructor(urn, data) {
        this.urn = urn;
        this.data = data;
    }

    get urnString() {
        return this.urn.toURNString();
    }

    matches(anotherTopic) {
        return this.urn.isEquivalent(anotherTopic.urn)
            && this.urn.resolves(this.data);
    }
}
