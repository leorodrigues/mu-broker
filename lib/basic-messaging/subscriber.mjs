export class Subscriber {
    constructor(topic, trampoline) {
        this.trampoline = trampoline;
        this.topic = topic;
    }

    listeningTo(topic) {
        return this.topic.matches(topic);
    }

    wakeUp() {
        return this.trampoline.spring();
    }
}
