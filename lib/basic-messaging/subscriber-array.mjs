export class SubscriberArray extends Array {
    findAllListeningTo(topic) {
        return this.filter(s => s.listeningTo(topic));
    }
}
