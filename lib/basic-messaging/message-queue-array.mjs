export class MessageQueueArray extends Array {
    post(topic, message) {
        const payload = typeof message.makeDeepCopy === 'function'
            ? message.makeDeepCopy()
            : message;
        for (const q of this)
            if (q.acceptsTopic(topic))
                q.push(payload);
    }
}
