import { AckError } from './ack-error.mjs';

const MSG_ERR_DECISION_PENDING = 'Acknowledgement decision is pending.';

const MSG_ERR_CANNOT_RESET = 'Acknowledgement cannot be reset.';

const UNDECIDED = 'undecided';

const CONFIRMED = 'confirmed';

const REFUSED = 'refused';

export class Acknowledgement {
    constructor() {
        Object.assign(this, { status: Acknowledgement.UNDECIDED });
    }

    get confirmed() {
        if (this.status === Acknowledgement.UNDECIDED)
            throw new AckError(MSG_ERR_DECISION_PENDING);
        return this.status === Acknowledgement.CONFIRMED;
    }

    get refused() {
        if (this.status === Acknowledgement.UNDECIDED)
            throw new AckError(MSG_ERR_DECISION_PENDING);
        return this.status === Acknowledgement.REFUSED;
    }

    confirm() {
        if (this.status !== Acknowledgement.UNDECIDED)
            throw new AckError(MSG_ERR_CANNOT_RESET);
        this.status = Acknowledgement.CONFIRMED;
    }

    refuse() {
        if (this.status !== Acknowledgement.UNDECIDED)
            throw new AckError(MSG_ERR_CANNOT_RESET);
        this.status = Acknowledgement.REFUSED;
    }

    static get UNDECIDED() {
        return UNDECIDED;
    }

    static get CONFIRMED() {
        return CONFIRMED;
    }

    static get REFUSED() {
        return REFUSED;
    }
}
