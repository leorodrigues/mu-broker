export class MuBroker {
    constructor(broadcaster, messageQueueArray, brokeringKit) {
        this.brokeringKit = brokeringKit;
        this.messageQueueArray = messageQueueArray;
        this.broadcaster = broadcaster;
    }

    async subscribe(topic, receiver) {
        const messageQueue = this.brokeringKit.newMessageQueue(topic);
        this.messageQueueArray.push(messageQueue);
        this.broadcaster.push(
            this.brokeringKit.newSubscriber(topic, receiver, messageQueue));
    }

    async publish(topic, message) {
        this.messageQueueArray.post(topic, message);
        this.broadcaster.broadcast(topic);
    }
}
