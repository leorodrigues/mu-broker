
import { Envelope } from '../basic-messaging/envelope.mjs';

export class Event extends Envelope {
    deriveEvent(eventBrokeringKit, { headers = { }, payload } = { }) {
        return eventBrokeringKit.newEvent({
            headers: this.deriveHeaders(headers),
            payload
        });
    }

    deriveCommand(
        eventBrokeringKit, responseTopic, { headers = { }, payload } = { }) {

        const derivedHeaders = this.deriveHeaders(headers);

        return eventBrokeringKit.newCommand({
            headers: { ...derivedHeaders, responseTopic }, payload
        });
    }

    deriveFireAndForgetCommand(
        eventBrokeringKit, { headers = { }, payload } = { }) {

        return eventBrokeringKit.newCommand({
            headers: this.deriveHeaders(headers), payload
        });
    }
}
