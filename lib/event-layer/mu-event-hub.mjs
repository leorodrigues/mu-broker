
export class MuEventHub {
    constructor(muCommandBus, eventBrokeringKit) {
        this.muCommandBus = muCommandBus;
        this.eventBrokeringKit = eventBrokeringKit;
    }

    invoke(commandTopic, responseTopic, { headers = { }, payload } = { }) {
        return this.muCommandBus.invoke(
            commandTopic, responseTopic, { payload, headers });
    }

    invokeAndForget(commandTopic, { headers = { }, payload } = { }) {
        return this.muCommandBus.invokeAndForget(
            commandTopic, { payload, headers });
    }

    invokeFromCommand(
        commandTopic, responseTopic, command, { headers, payload } = { }) {
        return this.muCommandBus.invokeFromCommand(
            commandTopic, responseTopic, command, { headers, payload });
    }

    invokeFromCommandAndForget(
        commandTopic, command, { headers, payload } = { }) {
        return this.muCommandBus.invokeFromCommandAndForget(
            commandTopic, command, { headers, payload });
    }

    invokeFromResponse(
        commandTopic, responseTopic, response, { headers, payload } = { }) {
        return this.muCommandBus.invokeFromResponse(
            commandTopic, responseTopic, response, { headers, payload });
    }

    invokeFromResponseAndForget(
        commandTopic, response, { headers, payload } = { }) {
        return this.muCommandBus.invokeFromResponseAndForget(
            commandTopic, response, { headers, payload });
    }

    async invokeFromEvent(
        commandTopic, responseTopic, event, { headers, payload } = { }) {

        const command = event.deriveCommand(
            this.eventBrokeringKit, responseTopic, { headers, payload });

        this.publish(commandTopic, command);

        return command.invocationId;
    }

    async invokeFromEventAndForget(
        commandTopic, event, { headers, payload } = { }) {

        const command = event.deriveFireAndForgetCommand(
            this.eventBrokeringKit, { headers, payload });

        this.publish(commandTopic, command);

        return command.invocationId;
    }

    respondTo(command, { headers = { }, payload } = { }) {
        return this.muCommandBus.respondTo(command, { payload, headers });
    }

    triggerFromEvent(eventTopic, event, { headers = { }, payload } = { }) {
        const kit = this.eventBrokeringKit;
        const newEvent = event.deriveEvent(kit, { headers, payload });
        return this.publish(eventTopic, newEvent);
    }

    triggerFromCommand(eventTopic, command, { headers = { }, payload } = { }) {
        const event = this.eventBrokeringKit.newEvent({
            headers: command.deriveHeaders(headers),
            payload
        });

        return this.publish(eventTopic, event);
    }

    trigger(eventTopic, { headers = { }, payload } = { }) {
        const event = this.eventBrokeringKit.newEvent({ payload, headers });
        return this.publish(eventTopic, event);
    }

    subscribe(topic, receiver) {
        return this.muCommandBus.subscribe(topic, receiver);
    }

    publish(topic, payload) {
        return this.muCommandBus.publish(topic, payload);
    }
}
