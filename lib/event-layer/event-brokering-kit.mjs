import { Event } from './event.mjs';
import { MuEventHub } from './mu-event-hub.mjs';

export class EventBrokeringKit {
    constructor(commandBrokeringKit, logger) {
        this.commandBrokeringKit = commandBrokeringKit;
        this.logger = logger;
    }

    newTopic(urnString, data) {
        return this.commandBrokeringKit.newTopic(urnString, data);
    }

    newTopicFromURN(urn, data) {
        return this.commandBrokeringKit.newTopicFromURN(urn, data);
    }

    newMessageQueue(topic) {
        return this.commandBrokeringKit.newMessageQueue(topic);
    }

    newSubscriber(topic, receiver, messageQueue) {
        return this.commandBrokeringKit.newSubscriber(
            topic, receiver, messageQueue);
    }

    newMuBroker() {
        return this.commandBrokeringKit.newMuBroker();
    }

    newMuCommandBus(muBroker) {
        return this.commandBrokeringKit.newMuCommandBus(muBroker);
    }

    newEventHub(muCommandBus) {
        return new MuEventHub(muCommandBus, this);
    }

    newEvent({ headers = { }, payload } = { }) {
        return new Event({ payload, headers });
    }

    newCommand({ headers = { }, payload } = { }) {
        return this.commandBrokeringKit.newCommand({ payload, headers });
    }

    newResponse({ headers = { }, payload } = { }) {
        return this.commandBrokeringKit.newResponse({ payload, headers });
    }

    newEnvelope({ headers = { }, payload } = { }) {
        return this.commandBrokeringKit.newEnvelope({ payload, headers });
    }
}