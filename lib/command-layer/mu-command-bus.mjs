export class MuCommandBus {
    constructor(muBroker, commandBrokeringKit) {
        this.muBroker = muBroker;
        this.commandBrokeringKit = commandBrokeringKit;
    }

    async invoke(
        commandTopic, responseTopic, { headers = { }, payload } = { }) {

        const command = this.commandBrokeringKit.newCommand({
            headers: { ...headers, responseTopic },
            payload
        });

        this.publish(commandTopic, command);

        return command.invocationId;
    }

    async invokeAndForget(
        commandTopic, { headers = { }, payload } = { }) {

        const command = this.commandBrokeringKit.newCommand({
            headers, payload
        });

        this.publish(commandTopic, command);
        return command.invocationId;
    }

    async invokeFromCommand(
        commandTopic,
        responseTopic,
        command,
        { headers = { }, payload } = { }
    ) {
        const newCommand = command.deriveCommand(
            this.commandBrokeringKit, responseTopic, { headers, payload });

        this.publish(commandTopic, newCommand);

        return newCommand.invocationId;
    }

    async invokeFromCommandAndForget(
        commandTopic, command, { headers = { }, payload } = { }) {

        const newCommand = command.deriveFireAndForgetCommand(
            this.commandBrokeringKit, { headers, payload });

        this.publish(commandTopic, newCommand);

        return newCommand.invocationId;
    }

    async invokeFromResponse(
        commandTopic,
        responseTopic,
        response,
        { headers = { }, payload } = { }
    ) {
        const command = response.deriveCommand(
            this.commandBrokeringKit, responseTopic, { headers, payload });

        this.publish(commandTopic, command);

        return command.invocationId;
    }

    async invokeFromResponseAndForget(
        commandTopic, response, { headers = { }, payload } = { }) {

        const command = response.deriveFireAndForgetCommand(
            this.commandBrokeringKit, { headers, payload });

        this.publish(commandTopic, command);

        return command.invocationId;
    }

    respondTo(command, { headers = { }, payload } = { }) {
        const kit = this.commandBrokeringKit;

        const params = { headers, payload };
        return command.withDerivedResponse(kit, params, (r, t) => {
            return this.publish(t, r);
        });
    }

    subscribe(topic, receiver) {
        return this.muBroker.subscribe(topic, receiver);
    }

    publish(topic, payload) {
        return this.muBroker.publish(topic, payload);
    }
}
