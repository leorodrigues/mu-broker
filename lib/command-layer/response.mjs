import { randomBytes } from 'node:crypto';

import { Envelope } from '../basic-messaging/envelope.mjs';

export class Response extends Envelope {
    constructor({ headers = { }, payload } = { }) {
        super({ payload, headers });

        this.ensureHeader('invocationId', () => {
            return `orphan:${randomBytes(8).toString('hex')}`;
        });
    }

    get invocationId() {
        return this.headers.invocationId;
    }

    deriveCommand(
        commandBrokeringKit,
        responseTopic,
        { headers = { }, payload } = {  }
    ) {
        const derivedHeaders = {
            ...this.deriveHeaders(headers, [ 'invocationId' ]),
            responseTopic
        };

        return commandBrokeringKit.newCommand({
            headers: derivedHeaders, payload
        });
    }

    deriveFireAndForgetCommand(
        commandBrokeringKit,
        { headers = { }, payload } = { }
    ) {
        const derivedHeaders = this.deriveHeaders(headers, [ 'invocationId' ]);

        return commandBrokeringKit.newCommand({
            headers: derivedHeaders, payload
        });
    }
}
