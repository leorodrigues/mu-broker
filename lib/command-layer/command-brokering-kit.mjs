import { Command } from './command.mjs';
import { Response } from './response.mjs';
import { MuCommandBus } from './mu-command-bus.mjs';

/* istanbul ignore next */
export class CommandBrokeringKit {
    constructor(brokeringKit) {
        this.brokeringKit = brokeringKit;
    }

    newTopic(urnString, data) {
        return this.brokeringKit.newTopic(urnString, data);
    }

    newTopicFromURN(urn, data) {
        return this.brokeringKit.newTopicFromURN(urn, data);
    }

    newMessageQueue(topic) {
        return this.brokeringKit.newMessageQueue(topic);
    }

    newSubscriber(topic, receiver, messageQueue) {
        return this.brokeringKit.newSubscriber(topic, receiver, messageQueue);
    }

    newMuBroker() {
        return this.brokeringKit.newMuBroker();
    }

    newMuCommandBus(muBroker) {
        return new MuCommandBus(muBroker, this);
    }

    newCommand({ headers = { }, payload } = { }) {
        return new Command({ payload, headers });
    }

    newResponse({ headers = { }, payload } = { }) {
        return new Response({ payload, headers });
    }

    newEnvelope({ headers = { }, payload } = { }) {
        return this.brokeringKit.newEnvelope({ payload, headers });
    }
}