import { Envelope } from '../basic-messaging/envelope.mjs';

const BLACK_LIST = [ 'responseTopic' ];

export class Command extends Envelope {
    constructor({ headers = { }, payload } = { }) {
        super({ payload, headers });
        this.ensureHeader('invocationId');
    }

    get invocationId() {
        return this.headers.invocationId;
    }

    deriveCommand(
        commandBrokeringKit, responseTopic, { headers = { }, payload } = { }) {

        const derivedHeaders = this.deriveHeaders(headers, [
            ...BLACK_LIST, 'invocationId'
        ]);

        return commandBrokeringKit.newCommand({
            headers: { ...derivedHeaders, responseTopic },
            payload
        });
    }

    deriveFireAndForgetCommand(
        commandBrokeringKit, { headers = { }, payload } = { }) {

        const derivedHeaders = this.deriveHeaders(headers, [
            ...BLACK_LIST, 'invocationId'
        ]);

        return commandBrokeringKit.newCommand({
            headers: derivedHeaders, payload
        });
    }

    async withDerivedResponse(
        commandBrokeringKit,
        { headers = { }, payload } = { },
        action = async () => { }
    ) {
        const topic = this.headers.responseTopic;
        if (isMissing(topic)) return;

        const newHeaders = this.deriveHeaders(headers);

        const response = commandBrokeringKit.newResponse({
            headers: newHeaders,
            payload
        });

        await action(response, topic);
    }

    deriveHeaders(headers, attributesToExclude = [ ]) {
        const derivedHeaders = super.deriveHeaders(headers, [
            ...attributesToExclude,
            ...BLACK_LIST,
            'invocationId'
        ]);

        derivedHeaders.invocationId = this.invocationId;

        return derivedHeaders;
    }
}

function isMissing(value) {
    return value === null
        || value === undefined
        || (typeof value === 'string' && value.trim() === '');
}
