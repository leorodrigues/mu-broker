import gulp from 'gulp';
import gulpClean from 'gulp-clean';
import { makeGulpReadme, declareTasks } from '@leorodrigues/gulp-readme';

const gReadme = makeGulpReadme();

const PROJECT_NAME = 'mu-broker';

const BUILD_PATH = 'build';

const JS_SOURCES = [
    'lib/**/*.mjs',
    '!lib/**/index.mjs'
];

const NOTES_SOURCES = [
    'docs/notes/foreword.txt',
    'docs/notes/warnings.txt',
    'docs/notes/resources.txt'
];

const ALL_SOURCES = {
    jsSources: JS_SOURCES,
    notesSources: NOTES_SOURCES,
    refSources: ['docs/notes/references.json']
};

const defaultDocsTask = declareTasks(
    PROJECT_NAME, gulp, gReadme, BUILD_PATH, ALL_SOURCES);

gulp.task(`${PROJECT_NAME}-docs`, gulp.series([defaultDocsTask], function() {
    return gulp.src(`${BUILD_PATH}/README.md`)
        .pipe(gulp.dest('./'));
}));

gulp.task('clean', function() {
    return gulp.src(`${BUILD_PATH}/`, {read: false})
        .pipe(gulpClean({force: true}));
});

gulp.task('default', gulp.series([`${PROJECT_NAME}-docs`]));
