# Mu Broker - A micro message broker

## Obtaining the software

Easy peasy...

```bash
$ git clone git@bitbucket.org:leorodrigues/mu-broker.git
```

## Sample usage

```javascript

const { BrokeringKit } = require('@leorodrigues/mu-broker');


const brokeringKit = new BrokeringKit({
    error: message => console.log(message)
});

const broker = brokeringKit.newMuBroker();

const echoTopic = brokeringKit.newTopic('urn:leonardo-org:events:echo');

broker.subscribe(echoTopic, {
    handle(evt, acknowledgement) {
        console.log(`Consumer A: ${evt.text}`);
        acknowledgement.confirm();
    }
});

broker.subscribe(echoTopic, {
    handle(evt, acknowledgement) {
        console.log(`Consumer B: ${evt.text}`);
        acknowledgement.confirm();
    }
});

broker.publish(echoTopic, { text: 'hello world' });
```